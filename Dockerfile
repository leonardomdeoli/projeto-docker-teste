FROM nginx:latest

WORKDIR /app

COPY html/ /usr/share/nginx/html

ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]